﻿using System.Security.Cryptography;
using System.Text;
using Konscious.Security.Cryptography;

namespace Listener.Services;

public static class CryptographyService
{
    public static async Task<string> EncryptPasswordAsync(string password, string salt)
    {
        var encrypt = new Argon2d(Encoding.Unicode.GetBytes(password));

        encrypt.Salt = Convert.FromBase64String(salt);
        encrypt.Iterations = 40;
        encrypt.DegreeOfParallelism = 16;
        encrypt.MemorySize = 8192;

        var encodedBytes = await encrypt.GetBytesAsync(128);
        return Convert.ToBase64String(encodedBytes);
    }

    public static string GenerateSalt()
    {
        var saltBytes = new byte[16];
        using var cryptoRng = RandomNumberGenerator.Create();
        cryptoRng.GetBytes(saltBytes);
        return Convert.ToBase64String(saltBytes);
    }
}