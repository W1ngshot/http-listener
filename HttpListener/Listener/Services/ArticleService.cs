﻿using Listener.CustomExceptions;
using Listener.Dto;
using Listener.Models.Main;
using Listener.Repositories;

namespace Listener.Services;

public static class ArticleService
{
    public static async Task<Article> GetArticleByIdAsync(string articleId)
    {
        var article = await ArticleRepository.GetArticleByIdAsync(articleId) ??
                      throw new NotFoundException("article");
        
        await ArticleRepository.AddViewToArticleAsync(articleId);
        return article;
    }

    public static async Task<List<Article>> GetCategoryArticlesAsync(string stringCategory)
    {
        if (!Enum.TryParse(typeof(Category), stringCategory, true, out var category))
            throw new ValidationException("Wrong category");
        
        var articles = await ArticleRepository.GetArticlesAsync();

        return articles.SortArticlesByCategory((Category)category);
    }

    public static async Task<List<Article>> GetSearchedArticlesAsync(string search, string stringCategory = "mostviewed")
    {
        if (!Enum.TryParse(typeof(Category), stringCategory, true, out var category))
            throw new ValidationException("Wrong category");

        var articles = await ArticleRepository.GetSearchedArticlesAsync(search);

        return articles.SortArticlesByCategory((Category) category);
    }

    private static List<Article> SortArticlesByCategory(this IEnumerable<Article> articles, Category category) =>
        category switch
        {
            Category.Popular => articles.OrderByDescending(article => article.Rate).ToList(),
            Category.New => articles.OrderByDescending(article => article.UploadDate).ToList(),
            Category.MostViewed => articles.OrderByDescending(article => article.Views).ToList(),
            _ => throw new ValidationException("Wrong category")
        };

    public static async Task<List<Article>> GetUserArticlesAsync(string userId)
    {
        var articles = await ArticleRepository.GetUserArticlesAsync(userId);

        return articles.ToList();
    }

    public static async Task<Article> AddArticleAsync(AddArticleDto addArticleDto, string? currentUserId)
    {
        if (string.IsNullOrWhiteSpace(currentUserId))
            throw new UnauthorizedException();
        
        ValidationService<AddArticleDto>.Validate(addArticleDto);

        var article = new Article
        {
            Id = Guid.NewGuid().ToString(),
            Title = addArticleDto.Title,
            ShortDescription = addArticleDto.ShortDescription,
            Text = addArticleDto.Text,
            TagId = addArticleDto.TagId,
            UploadDate = DateTime.Now,
            UserId = currentUserId,
            AssetLink = addArticleDto.AssetLink,
            Views = 0
        };

        await ArticleRepository.AddArticleAsync(article);
        return article;
    }

    public static async Task<Article> UpdateArticleAsync(UpdateArticleDto updateArticleDto, string? currentUserId)
    {
        if (string.IsNullOrWhiteSpace(currentUserId))
            throw new UnauthorizedException();

        ValidationService<UpdateArticleDto>.Validate(updateArticleDto);

        var article = await ArticleRepository.GetArticleByIdAsync(updateArticleDto.Id) ??
                      throw new NotFoundException("article");

        if (article.UserId != currentUserId)
            throw new ValidationException("Not allowed");

        article.Title = updateArticleDto.Title;
        article.ShortDescription = updateArticleDto.ShortDescription;
        article.Text = updateArticleDto.Text;
        article.TagId = updateArticleDto.TagId;
        article.AssetLink = updateArticleDto.AssetLink;

        await ArticleRepository.UpdateArticleAsync(article);
        return article;
    }

    public static async Task DeleteArticleAsync(string articleId , string? currentUserId)
    {
        if (string.IsNullOrWhiteSpace(currentUserId))
            throw new UnauthorizedException();

        var article = await ArticleRepository.GetArticleByIdAsync(articleId) ??
                      throw new NotFoundException("article");
        
        if (article.UserId != currentUserId)
            throw new ValidationException("Not allowed");

        await ArticleRepository.DeleteArticleAsync(articleId);
    }
}