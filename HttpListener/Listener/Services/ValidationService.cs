﻿using System.ComponentModel.DataAnnotations;
using ValidationException = Listener.CustomExceptions.ValidationException;

namespace Listener.Services;

public static class ValidationService<T> where T : class
{
    public static void Validate(T model)
    {
        var results = new List<ValidationResult>();
        if (!Validator.TryValidateObject(model, new ValidationContext(model), results, true))
            throw new ValidationException(results);
    }
}