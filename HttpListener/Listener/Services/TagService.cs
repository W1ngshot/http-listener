﻿using Listener.Models.Main;
using Listener.Repositories;

namespace Listener.Services;

public static class TagService
{
    public static async Task<List<Tag>> GetAllTags()
    {
        var tags = await TagRepository.GetTagsAsync();

        return tags.ToList();
    }
}