﻿using Listener.CustomExceptions;
using Listener.Dto;
using Listener.Models.Main;
using Listener.Repositories;

namespace Listener.Services;

public static class CommentService
{
    public static async Task<List<Comment>> GetArticleCommentsAsync(string articleId)
    {
        var comments = await CommentRepository.GetArticleCommentsAsync(articleId);

        return comments.OrderByDescending(comment => comment.Date).ToList();
    }

    public static async Task<Comment> AddCommentAsync(AddCommentDto addCommentDto, string? currentUserId)
    {
        if (string.IsNullOrWhiteSpace(currentUserId))
            throw new UnauthorizedException();
        
        ValidationService<AddCommentDto>.Validate(addCommentDto);

        var user = await UserService.GetUserByIdAsync(currentUserId) ??
                   throw new ValidationException("Wrong user");
        
        var comment = new Comment
        {
            Id = Guid.NewGuid().ToString(),
            ArticleId = addCommentDto.ArticleId,
            Date = DateTime.Now,
            Message = addCommentDto.Message,
            UserId = user.Id,
            UserNickname = user.Nickname
        };

        await CommentRepository.AddCommentAsync(comment);
        return comment;
    }

    public static async Task<Comment> EditCommentAsync(EditCommentDto editCommentDto, string? currentUserId)
    {
        if (string.IsNullOrWhiteSpace(currentUserId))
            throw new UnauthorizedException();
        
        ValidationService<EditCommentDto>.Validate(editCommentDto);

        var comment = await CommentRepository.GetCommentByIdAsync(editCommentDto.Id) ??
                      throw new NotFoundException("comment");

        if (comment.UserId != currentUserId)
            throw new ValidationException("Not allowed");

        comment.Message = editCommentDto.Message;
        await CommentRepository.EditCommentAsync(comment);
        return comment;
    }

    public static async Task DeleteCommentAsync(string commentId, string? currentUserId)
    {
        if (string.IsNullOrWhiteSpace(currentUserId))
            throw new UnauthorizedException();

        var comment = await CommentRepository.GetCommentByIdAsync(commentId) ??
                      throw new NotFoundException("comment");

        if (comment.UserId != currentUserId)
            throw new ValidationException("Not allowed");

        await CommentRepository.DeleteCommentAsync(comment);
    }
}