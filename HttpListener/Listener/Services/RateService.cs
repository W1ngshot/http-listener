﻿using Listener.CustomExceptions;
using Listener.Dto;
using Listener.Models.Main;
using Listener.Repositories;

namespace Listener.Services;

public static class RateService
{
    public static async Task<int> GetUserArticleRate(string articleId, string? userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new UnauthorizedException();

        return await RateRepository.GetUserArticleRatingAsync(articleId, userId);
    }

    public static async Task<int> AddOrUpdateRateAsync(ChangeRateDto changeRateDto, string? currentUserId)
    {
        if (string.IsNullOrWhiteSpace(currentUserId))
            throw new UnauthorizedException();
            
        ValidationService<ChangeRateDto>.Validate(changeRateDto);

        var rate = await RateRepository.GetUserArticleRateAsync(changeRateDto.ArticleId, currentUserId);
        
        if (rate is null)
        {
            await RateRepository.AddRate(new Rate
            {
                Id = Guid.NewGuid().ToString(),
                ArticleId = changeRateDto.ArticleId,
                Rating = changeRateDto.Rating,
                UserId = currentUserId
            });
        }
        else
        {
            rate.Rating = changeRateDto.Rating;
            await RateRepository.UpdateRate(rate);
        }
        return changeRateDto.Rating;
    }
}