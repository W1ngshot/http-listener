﻿using System.Net;
using System.Text;
using System.Text.Json;

namespace Listener.Services;

public static class ReturnService<T> where T : class
{
    public static async Task Return(HttpListenerContext context, T model,
        ContentTypes type = ContentTypes.Json, int statusCode = 200)
    {
        context.Response.StatusCode = statusCode;
        var encoding = context.Response.ContentEncoding = Encoding.UTF8;
        context.Response.ContentType = type == ContentTypes.Json ? "application/json" : "text/plain";
        var text = type == ContentTypes.Json ? JsonSerializer.Serialize(model) : model.ToString();
        
        await context.Response.OutputStream.WriteAsync(encoding.GetBytes(text ?? ""));
        context.Response.OutputStream.Close();
    }
}

public enum ContentTypes
{
    Json,
    Text
}