﻿using Listener.CustomExceptions;
using Listener.Dto;
using Listener.Models.Main;
using Listener.Repositories;
using ValidationException = Listener.CustomExceptions.ValidationException;

namespace Listener.Services;

public static class UserService
{
    public static async Task Register(RegisterDto registerDto)
    {
        ValidationService<RegisterDto>.Validate(registerDto);
        if (!await UserRepository.EnsureNicknameAndEmailIsAvailable(registerDto.Nickname, registerDto.Email))
            throw new ValidationException("Nickname or email is already taken");
        
        var newSalt = CryptographyService.GenerateSalt();
        var passwordHash = await CryptographyService.EncryptPasswordAsync(registerDto.Password, newSalt);

        await UserRepository.AddUser(new User
        {
            Id = Guid.NewGuid().ToString(),
            Email = registerDto.Email,
            Nickname = registerDto.Nickname,
            PasswordSalt = newSalt,
            PasswordHash = passwordHash,
            RegistrationDate = DateTime.Now
        });
    }

    public static async Task<User> Login(LoginDto loginDto)
    {
        var user = await UserRepository.GetUserByEmailAsync(loginDto.Email) ??
                   throw new ValidationException("Wrong password or email");
        
        var dtoPasswordHash = await CryptographyService.EncryptPasswordAsync(loginDto.Password, user.PasswordSalt);

        if (dtoPasswordHash != user.PasswordHash)
            throw new ValidationException("Wrong password or email");
        
        return user;
    }

    public static async Task<User> GetUserByIdAsync(string userId)
    {
        return await UserRepository.GetUserByIdAsync(userId) ??
                   throw new NotFoundException("user");
    }

    public static async Task UpdateUser(UpdateUserDto updateDto, string? currentUserId)
    {
        if (currentUserId is null || updateDto.Id != currentUserId)
            throw new UnauthorizedException();
        
        ValidationService<UpdateUserDto>.Validate(updateDto);
        if (!await UserRepository.EnsureNicknameAndEmailIsAvailable(updateDto.Nickname, updateDto.Email, currentUserId))
            throw new ValidationException("Nickname or email is already taken");
        
        var user = await UserRepository.GetUserByIdAsync(updateDto.Id) 
                   ?? throw new NotFoundException("user");
        user.Email = updateDto.Email;
        user.Nickname = updateDto.Nickname;

        await UserRepository.UpdateUser(user);
    }

    public static async Task ChangePassword(ChangePasswordDto passwordDto, string? currentUserId)
    {
        if (currentUserId is null)
            throw new UnauthorizedException();

        var user = await UserRepository.GetUserByIdAsync(currentUserId) ??
                   throw new ValidationException("User not found");
        
        ValidationService<ChangePasswordDto>.Validate(passwordDto);
        var oldPassword = await CryptographyService.EncryptPasswordAsync(passwordDto.OldPassword, user.PasswordSalt);
        if (oldPassword != user.PasswordHash)
            throw new ValidationException("Wrong old password");

        user.PasswordHash = await CryptographyService.EncryptPasswordAsync(passwordDto.NewPassword, user.PasswordSalt);
        await UserRepository.UpdateUser(user);
    }
}