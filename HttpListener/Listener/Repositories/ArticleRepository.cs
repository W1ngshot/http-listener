﻿using System.Data;
using Dapper;
using Listener.Models.Main;
using Npgsql;

namespace Listener.Repositories;

public static class ArticleRepository
{
    private static readonly string ConnectionString = Configuration.GetConnectionString();

    public static async Task<Article?> GetArticleByIdAsync(string id)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"select
        articles.id as {nameof(Article.Id)},
        title as {nameof(Article.Title)},
        short_description as {nameof(Article.ShortDescription)},
        text as {nameof(Article.Text)},
        t.id as {nameof(Article.TagId)},
        t.name as {nameof(Article.TagName)},
        upload_date as {nameof(Article.UploadDate)},
        user_id as {nameof(Article.UserId)},
        asset_link as {nameof(Article.AssetLink)},
        views as {nameof(Article.Views)},
        (select sum(rating) / count(*)
         from rates
         where rates.article_id = articles.id) as {nameof(Article.Rate)}
        from articles
        inner join tags t on articles.tag = t.id
        where articles.id = @Id";
        
        return await connection.QueryFirstOrDefaultAsync<Article>(query, new {Id = id});
    }

    public static async Task<IEnumerable<Article>> GetArticlesAsync()
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"select
        articles.id as {nameof(Article.Id)},
        title as {nameof(Article.Title)},
        short_description as {nameof(Article.ShortDescription)},
        text as {nameof(Article.Text)},
        t.id as {nameof(Article.TagId)},
        t.name as {nameof(Article.TagName)},
        upload_date as {nameof(Article.UploadDate)},
        user_id as {nameof(Article.UserId)},
        asset_link as {nameof(Article.AssetLink)},
        views as {nameof(Article.Views)},
        (select sum(rating) / count(*)
         from rates
         where rates.article_id = articles.id) as {nameof(Article.Rate)}
        from articles
        inner join tags t on articles.tag = t.id";

        return await connection.QueryAsync<Article>(query);
    }

    public static async Task<IEnumerable<Article>> GetSearchedArticlesAsync(string search)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);
        
        const string query = $@"select
        articles.id as {nameof(Article.Id)},
        title as {nameof(Article.Title)},
        short_description as {nameof(Article.ShortDescription)},
        text as {nameof(Article.Text)},
        t.id as {nameof(Article.TagId)},
        t.name as {nameof(Article.TagName)},
        upload_date as {nameof(Article.UploadDate)},
        user_id as {nameof(Article.UserId)},
        asset_link as {nameof(Article.AssetLink)},
        views as {nameof(Article.Views)},
        (select sum(rating) / count(*)
         from rates
         where rates.article_id = articles.id) as {nameof(Article.Rate)}
        from articles
        inner join tags t on articles.tag = t.id
        where position(@Search in lower(title)) > 0";

        return await connection.QueryAsync<Article>(query, new {Search = search});
    }

    public static async Task<IEnumerable<Article>> GetUserArticlesAsync(string userId)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"select
        articles.id as {nameof(Article.Id)},
        title as {nameof(Article.Title)},
        short_description as {nameof(Article.ShortDescription)},
        text as {nameof(Article.Text)},
        t.id as {nameof(Article.TagId)},
        t.name as {nameof(Article.TagName)},
        upload_date as {nameof(Article.UploadDate)},
        user_id as {nameof(Article.UserId)},
        asset_link as {nameof(Article.AssetLink)},
        views as {nameof(Article.Views)},
        (select sum(rating) / count(*)
         from rates
         where rates.article_id = articles.id) as {nameof(Article.Rate)}
        from articles
        inner join tags t on articles.tag = t.id
        where user_id = @UserId";

        return await connection.QueryAsync<Article>(query, new {UserId = userId});
    }

    public static async Task AddArticleAsync(Article article)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"insert into articles
        (id, title, short_description, text, tag, upload_date, user_id, asset_link, views) 
        values
        (@{nameof(Article.Id)},
         @{nameof(Article.Title)},
         @{nameof(Article.ShortDescription)},
         @{nameof(Article.Text)},
         @{nameof(Article.TagId)},
         @{nameof(Article.UploadDate)},
         @{nameof(Article.UserId)},
         @{nameof(Article.AssetLink)},
         @{nameof(Article.Views)})";

        await connection.ExecuteAsync(query, article);
    }

    public static async Task UpdateArticleAsync(Article article)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"update articles set 
                    title = @{nameof(Article.Title)},
                    short_description = @{nameof(Article.ShortDescription)},
                    text = @{nameof(Article.Text)},
                    tag = @{nameof(Article.TagId)},
                    asset_link = @{nameof(Article.AssetLink)}
                    where id = @{nameof(Article.Id)}";

        await connection.ExecuteAsync(query, article);
    }

    public static async Task AddViewToArticleAsync(string articleId)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"update articles set 
                    views = views + 1
                    where id = @ArticleId";

        await connection.ExecuteAsync(query, new { ArticleId = articleId});
    }

    public static async Task DeleteArticleAsync(string articleId)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"delete from articles
            where id = @ArticleID";

        await connection.ExecuteAsync(query, new {ArticleId = articleId});
    }
}