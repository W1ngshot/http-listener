﻿using System.Data;
using Dapper;
using Listener.Models.Main;
using Npgsql;

namespace Listener.Repositories;

public static class TagRepository
{
    private static readonly string ConnectionString = Configuration.GetConnectionString();
    
    public static async Task<IEnumerable<Tag>> GetTagsAsync()
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"select
            id as {nameof(Tag.Id)},
            name as {nameof(Tag.Name)}
            from tags";

        return await connection.QueryAsync<Tag>(query);
    }
}