﻿using System.Data;
using Dapper;
using Listener.Models.Main;
using Npgsql;

namespace Listener.Repositories;

public static class UserRepository
{
    private static readonly string ConnectionString = Configuration.GetConnectionString();
    
    public static async Task<User?> GetUserByIdAsync(string id)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);
        
        const string query = $@"select
            id as {nameof(User.Id)},
            email as {nameof(User.Email)},
            registration_date as {nameof(User.RegistrationDate)},
            nickname as {nameof(User.Nickname)},
            password_hash as {nameof(User.PasswordHash)},
            password_salt as {nameof(User.PasswordSalt)}
            from users
            where id = @Id";
        
        return await connection.QueryFirstOrDefaultAsync<User>(query, new {Id = id});
    }

    public static async Task<User?> GetUserByEmailAsync(string email)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);
        
        const string query = $@"select
            id as {nameof(User.Id)},
            email as {nameof(User.Email)},
            registration_date as {nameof(User.RegistrationDate)},
            nickname as {nameof(User.Nickname)},
            password_hash as {nameof(User.PasswordHash)},
            password_salt as {nameof(User.PasswordSalt)}
            from users
            where email = @Email";
        
        return await connection.QueryFirstOrDefaultAsync<User>(query, new {Email = email});
    }

    public static async Task<string> AddUser(User user)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"insert into users values
            (@{nameof(User.Id)},
            @{nameof(User.Email)},
            @{nameof(User.RegistrationDate)},
            @{nameof(User.Nickname)},
            @{nameof(User.PasswordHash)},
            @{nameof(User.PasswordSalt)})
            returning id";

        return await connection.QueryFirstAsync<string>(query, user);
    }

    public static async Task UpdateUser(User user)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);
        
        const string query = $@"update users 
            set email = @{nameof(User.Email)},
                nickname = @{nameof(User.Nickname)},
                password_hash = @{nameof(User.PasswordHash)}
            where id = @{nameof(User.Id)}";

        await connection.ExecuteAsync(query, user);
    }

    public static async Task<bool> EnsureNicknameAndEmailIsAvailable(string nickname, string email,
        string currentUserId = "")
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"select count(*) = 0
            from users
            where (nickname = @Nickname or email = @Email) and id <> @Id";

        return await connection
            .QueryFirstAsync<bool>(query, new {Nickname = nickname, Email = email, Id = currentUserId});
    }
}