﻿using System.Data;
using Dapper;
using Listener.Models.Main;
using Npgsql;

namespace Listener.Repositories;

public static class CommentRepository
{
    private static readonly string ConnectionString = Configuration.GetConnectionString();
    
    public static async Task<IEnumerable<Comment>> GetArticleCommentsAsync(string articleId)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"select
            comments.id as {nameof(Comment.Id)},
            message as {nameof(Comment.Message)},
            date as {nameof(Comment.Date)},
            user_id as {nameof(Comment.UserId)},
            nickname as {nameof(Comment.UserNickname)}
            from comments
            inner join users u on u.id = comments.user_id
            where article_id = @ArticleId";

        return await connection.QueryAsync<Comment>(query, new {ArticleId = articleId});
    }
    
    public static async Task<Comment?> GetCommentByIdAsync(string commentId)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"select
            id as {nameof(Comment.Id)},
            message as {nameof(Comment.Message)},
            date as {nameof(Comment.Date)},
            user_id as {nameof(Comment.UserId)}
            from comments
            where id = @CommentId";

        return await connection.QueryFirstOrDefaultAsync<Comment>(query, new {CommentId = commentId});
    }

    public static async Task AddCommentAsync(Comment comment)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"insert into comments (id, message, date, user_id, article_id)
            values 
            (@{nameof(Comment.Id)},
             @{nameof(Comment.Message)},
             @{nameof(Comment.Date)},
             @{nameof(Comment.UserId)},
             @{nameof(Comment.ArticleId)})";

        await connection.ExecuteAsync(query, comment);
    }

    public static async Task EditCommentAsync(Comment comment)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"update comments set
            message = @{nameof(Comment.Message)}
            where id = @{nameof(Comment.Id)}";

        await connection.ExecuteAsync(query, comment);
    }

    public static async Task DeleteCommentAsync(Comment comment)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"delete from comments
            where id = @{nameof(Comment.Id)}";

        await connection.ExecuteAsync(query, comment);
    }
}