﻿using System.Data;
using Dapper;
using Listener.Models.Main;
using Npgsql;

namespace Listener.Repositories;

public static class RateRepository
{
    private static readonly string ConnectionString = Configuration.GetConnectionString();
    
    public static async Task<int> GetUserArticleRatingAsync(string articleId, string userId)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"select
            rating
            from rates
            where user_id = @UserId and article_id = @ArticleId";

        return await connection.QueryFirstOrDefaultAsync<int>(query, 
            new {ArticleId = articleId, UserId = userId});
    }
    
    public static async Task<Rate?> GetUserArticleRateAsync(string articleId, string userId)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"select
            id as {nameof(Rate.Id)},
            rating as {nameof(Rate.Rating)},
            article_id as {nameof(Rate.ArticleId)},
            user_id as {nameof(Rate.UserId)}
            from rates
            where user_id = @UserId and article_id = @ArticleId";

        return await connection.QueryFirstOrDefaultAsync<Rate>(query, 
            new {ArticleId = articleId, UserId = userId});
    }

    public static async Task AddRate(Rate rate)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"insert into rates (id, rating, user_id, article_id) 
            values
            (@{nameof(Rate.Id)},
             @{nameof(Rate.Rating)},
             @{nameof(Rate.UserId)},
             @{nameof(Rate.ArticleId)})";

        await connection.ExecuteAsync(query, rate);
    }

    public static async Task UpdateRate(Rate rate)
    {
        using IDbConnection connection = new NpgsqlConnection(ConnectionString);

        const string query = $@"update rates set
             rating = @{nameof(Rate.Rating)}
             where user_id = @{nameof(Rate.UserId)} and article_id = @{nameof(Rate.ArticleId)}";

        await connection.ExecuteAsync(query, rate);
    }
}