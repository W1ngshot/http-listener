﻿using System.Net;
using System.Reflection;
using Listener.Controllers;
using Listener.CustomExceptions;
using Listener.Routing;

namespace Listener.Handlers;

public static class RouteHandler
{
    private static IEnumerable<MethodInfo> Methods =>
        Assembly.GetExecutingAssembly()
            .GetTypes()
            .Where(type => typeof(IController)
                .IsAssignableFrom(type))
            .SelectMany(type => type.GetMethods()
                .Where(method => method.GetCustomAttributes(true).Any(attr => attr is MappingAttribute)));
    
    private static MethodInfo DefaultMethod =>
        Methods.SingleOrDefault(method => method
            .GetCustomAttributes(true)
            .Any(y => y is MappingAttribute {Method: "*", Route: "*"}))!;
    
    public static async Task HandleRoute(this HttpListenerContext context)
    {
        var isAuthorized = context.Request.Headers["user-id"] is not null;
        var requiredMethod = GetRequiredMethod(context);
        
        if (requiredMethod is not null)
        {
            if (requiredMethod.GetCustomAttributes(true)
                .Any(attr => attr is MappingAttribute {IsAuthorizeRequired: true} && !isAuthorized))
            {
                throw new UnauthorizedException();
            }

            await ((Task) requiredMethod.Invoke(
                ControllerBuilder.Build<IController>(requiredMethod.DeclaringType!),
                new object[] {context})!).ConfigureAwait(false);
        }
        else
            await ((Task) DefaultMethod.Invoke(
                ControllerBuilder.Build<IController>(DefaultMethod.DeclaringType!),
                new object[] {context})!).ConfigureAwait(false);
    }

    private static MethodInfo? GetRequiredMethod(HttpListenerContext context)
    {
        var route = context.Request.Url?.Segments[1].Replace("/", "");
        
        return Methods.SingleOrDefault(x => x
            .GetCustomAttributes(true)
            .Any(attr => attr is MappingAttribute mapping &&
                         mapping.Method == context.Request.HttpMethod &&
                         mapping.Route == route));
    }
}