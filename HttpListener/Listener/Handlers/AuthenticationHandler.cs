﻿using System.Net;

namespace Listener.Handlers;

public static class AuthenticationHandler
{
    public static async Task HandleAuthentication(this HttpListenerContext context)
    {
        var sessionId = context.Request.Cookies["session-id"]?.Value;
        if (sessionId is not null)
        {
            var userId = await RedisContext.Redis.StringGetAsync(sessionId);
            if (userId.HasValue)
            {
                context.Request.Headers.Add("user-id", userId.ToString());
            }
        }

        await context.HandleRoute();
    }
}