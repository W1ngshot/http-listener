﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Text;
using Listener.CustomExceptions;
using Listener.Services;
using ValidationException = Listener.CustomExceptions.ValidationException;

namespace Listener.Handlers;

public static class ExceptionHandler
{
    public static async Task HandleExceptions(this HttpListenerContext context)
    {
        try
        {
            await context.HandleAuthentication();
        }
        catch (ValidationException exception)
        {
            if (exception.ExceptionMessages is not null)
                await context.ReturnException(exception.ExceptionMessages, 400);
            else
                await context.ReturnException(exception.Message, 400);
        }
        catch (UnauthorizedException exception)
        {
            await context.ReturnException(exception.Message, 401);
        }
        catch (NotFoundException exception)
        {
            await context.ReturnException(exception.Message, 404);
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception);
            await context.ReturnException("Internal server error", 500);
        }
    }

    private static async Task ReturnException(this HttpListenerContext context, string message, int statusCode)
    {
        await ReturnService<string>.Return(context, message, ContentTypes.Text, statusCode);
    }
    
    private static async Task ReturnException(this HttpListenerContext context, List<ValidationResult> results, int statusCode, bool isShowing = false)
    {
        var strBuilder = new StringBuilder();
        foreach (var result in results)
        {
            strBuilder.AppendLine(result.ErrorMessage);
        }
        
        await ReturnService<string>.Return(context, strBuilder.ToString(), ContentTypes.Text, statusCode);
    }
}