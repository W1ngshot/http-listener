﻿using StackExchange.Redis;

namespace Listener;

public class RedisContext
{
    private static readonly Lazy<ConnectionMultiplexer> LazyConnection = new (
        () => ConnectionMultiplexer.Connect(new ConfigurationOptions 
        { 
            EndPoints =
            {
                "localhost:6379"
            }
        }));

    private static ConnectionMultiplexer Connection => LazyConnection.Value;

    public static IDatabase Redis => Connection.GetDatabase();
}
