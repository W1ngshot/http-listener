﻿namespace Listener;

public enum Category
{
    Popular,
    New,
    MostViewed
}