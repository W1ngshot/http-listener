﻿using System.ComponentModel.DataAnnotations;

namespace Listener.Dto;

public class EditCommentDto
{
    [Required(ErrorMessage = "CommentId is required")] 
    public string Id { get; set; } = null!;

    [Required(ErrorMessage = "Message is required")] 
    public string Message { get; set; } = null!;
}