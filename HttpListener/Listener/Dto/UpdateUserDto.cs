﻿using System.ComponentModel.DataAnnotations;

namespace Listener.Dto;

public class UpdateUserDto
{
    public string Id { get; set; } = null!;
    
    [Required(ErrorMessage = "Email is required")] 
    [EmailAddress(ErrorMessage = "Wrong email address")] 
    public string Email { get; set; } = null!;
    
    [Required(ErrorMessage = "Email is required")]
    [StringLength(25, MinimumLength = 4, ErrorMessage = "Wrong nickname length")]
    [RegularExpression(@"[\d||\w]*", ErrorMessage = "Nickname contains wrong symbols")]
    public string Nickname { get; set; } = null!;
}