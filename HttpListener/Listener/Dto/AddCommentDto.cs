﻿using System.ComponentModel.DataAnnotations;

namespace Listener.Dto;

public class AddCommentDto
{
    [Required(ErrorMessage = "ArticleId is required")]
    public string ArticleId { get; set; } = null!;

    [Required(ErrorMessage = "Message is required")]
    public string Message { get; set; } = null!;
}