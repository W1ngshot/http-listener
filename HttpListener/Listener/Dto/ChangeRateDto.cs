﻿using System.ComponentModel.DataAnnotations;

namespace Listener.Dto;

public class ChangeRateDto
{
    [Required(ErrorMessage = "ArticleId is required")]
    public string ArticleId { get; set; } = null!;
    
    [Required(ErrorMessage = "Rate is required")]
    [Range(1, 5, ErrorMessage = "Wrong rate value")]
    public int Rating { get; set; }
}