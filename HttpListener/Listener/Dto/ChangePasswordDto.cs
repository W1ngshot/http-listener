﻿using System.ComponentModel.DataAnnotations;

namespace Listener.Dto;

public class ChangePasswordDto
{
    [Required(ErrorMessage = "Old password is required")] 
    public string OldPassword { get; set; } = null!;

    [Required(ErrorMessage = "New Password is required")] 
    [StringLength(60, MinimumLength = 8, ErrorMessage = "Wrong new password length")]
    public string NewPassword { get; set; } = null!;
}