﻿using System.ComponentModel.DataAnnotations;

namespace Listener.Dto;

public class RegisterDto
{
    [Required(ErrorMessage = "Email is required")] 
    [EmailAddress(ErrorMessage = "Wrong email address")] 
    public string Email { get; set; } = null!;

    [Required(ErrorMessage = "Nickname is required")]
    [StringLength(25, MinimumLength = 4, ErrorMessage = "Wrong nickname length")]
    [RegularExpression(@"[\d||\w]*", ErrorMessage = "Nickname contains wrong symbols")]
    public string Nickname { get; set; } = null!;

    [Required(ErrorMessage = "Password is required")]
    [StringLength(60, MinimumLength = 8, ErrorMessage = "Wrong password length")]
    public string Password { get; set; } = null!;
}