﻿using System.ComponentModel.DataAnnotations;

namespace Listener.Dto;

public class AddArticleDto
{
    [Required(ErrorMessage = "Title is required")]
    [StringLength(60, MinimumLength = 3, ErrorMessage = "Wrong title length")]
    [RegularExpression(@"[\d||\w||\s]*", ErrorMessage = "Title contains wrong symbols")]
    public string Title { get; set; } = null!;
    
    public string? ShortDescription { get; set; }
    
    [Required(ErrorMessage = "Text is required")]
    public string Text { get; set; } = null!;
    
    [Required(ErrorMessage = "Tag is required")]
    public string TagId { get; set; } = null!;
    
    public string? AssetLink { get; set; }
}