﻿namespace Listener.Routing;

[AttributeUsage(AttributeTargets.Method)]
public class MappingAttribute : Attribute
{
    public readonly string Route;
    public readonly string Method;
    public readonly bool IsAuthorizeRequired;

    public MappingAttribute(string route, string method, bool isAuthorizeRequired = false)
    {
        Route = route;
        Method = method;
        IsAuthorizeRequired = isAuthorizeRequired;
    }
}