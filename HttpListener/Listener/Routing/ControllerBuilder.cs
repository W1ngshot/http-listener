﻿using System.Collections.Concurrent;
using System.Linq.Expressions;

namespace Listener.Routing;

public static class ControllerBuilder
{
    private delegate T ObjectActivator<T>(params object[] args);
    private static readonly ConcurrentDictionary<object, object> ActivatorCache = new();

    public static T Build<T>(Type type)
    {
        if (ActivatorCache.ContainsKey(type))
        {
            var activator = (ActivatorCache[type] as ObjectActivator<T>)!;
            return activator();
        }
        
        var parameters = Expression.Parameter(typeof(object[]), "args");
        var newExpression = Expression.New(type);
        var lambdaExpression = Expression.Lambda(typeof(ObjectActivator<T>), newExpression, parameters);
        var compiledExpression = (ObjectActivator<T>)lambdaExpression.Compile();
        ActivatorCache[type] = compiledExpression;
        return compiledExpression();
    }
}