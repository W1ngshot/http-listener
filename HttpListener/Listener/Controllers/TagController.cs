﻿using System.Net;
using Listener.Models.Main;
using Listener.Routing;
using Listener.Services;

namespace Listener.Controllers;

public class TagController : IController
{
    [Mapping("tags", "GET")]
    public async Task GetTags(HttpListenerContext context)
    {
        var tags = await TagService.GetAllTags();
        await ReturnService<List<Tag>>.Return(context, tags);
    }
}