﻿using System.Net;
using System.Text.Json;
using Listener.CustomExceptions;
using Listener.Dto;
using Listener.Routing;
using Listener.Services;

namespace Listener.Controllers;

public class RateController : IController
{
    [Mapping("rate", "GET", true)]
    public async Task GetCurrentUserArticleRate(HttpListenerContext context)
    {
        var articleId = context.Request.QueryString["article-id"] ??
                        throw new ValidationException("article-id is missing");
        
        var currentUserId = context.Request.Headers["user-id"];

        var rate = await RateService.GetUserArticleRate(articleId, currentUserId);
        await ReturnService<string>.Return(context, rate.ToString());
    }

    [Mapping("rate_change", "POST", true)]
    public async Task ChangeRate(HttpListenerContext context)
    {
        using var data = new StreamReader(context.Request.InputStream);
        var changeRateDto = JsonSerializer.Deserialize<ChangeRateDto>(await data.ReadToEndAsync());
        var currentUserId = context.Request.Headers["user-id"];
        
        var rate = await RateService.AddOrUpdateRateAsync(changeRateDto, currentUserId);
        await ReturnService<string>.Return(context, rate.ToString());
    }
}