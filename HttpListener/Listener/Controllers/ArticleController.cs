﻿using System.Net;
using System.Text.Json;
using Listener.CustomExceptions;
using Listener.Dto;
using Listener.Models.Main;
using Listener.Models.Public;
using Listener.Routing;
using Listener.Services;

namespace Listener.Controllers;

public class ArticleController : IController
{
    [Mapping("*", "*")]
    public Task GeneralMethod(HttpListenerContext context)
    {
        throw new ValidationException("Wrong route");
    }

    [Mapping("articles_category", "GET")]
    public async Task GetCategoryArticles(HttpListenerContext context)
    {
        var category = context.Request.QueryString["category"] ??
                       throw new ValidationException("category is missing");

        var articles = await ArticleService.GetCategoryArticlesAsync(category);
        
        await ReturnService<List<Article>>.Return(context, articles);
    }

    [Mapping("articles_search", "GET")]
    public async Task SearchArticles(HttpListenerContext context)
    {
        var search = context.Request.QueryString["search"] ??
                     throw new ValidationException("search is missing");
        var category = context.Request.QueryString["category"] ??
                       throw new ValidationException("category is missing");

        var articles = await ArticleService.GetSearchedArticlesAsync(search, category);

        await ReturnService<List<Article>>.Return(context, articles);
    }
    
    [Mapping("article", "GET")]
    public async Task GetArticleById(HttpListenerContext context)
    {
        var articleId = context.Request.QueryString["article-id"];
        if (articleId is null)
            throw new ValidationException("article-id is missing");

        var article = await ArticleService.GetArticleByIdAsync(articleId);

        await ReturnService<PublicArticle>.Return(context, article.ToPublic());
    }

    [Mapping("articles_user", "GET")]
    public async Task GetUserArticles(HttpListenerContext context)
    {
        var userId = context.Request.QueryString["user-id"];
        if (userId is null)
            throw new ValidationException("user-id is missing");

        var articles = await ArticleService.GetUserArticlesAsync(userId);
        await ReturnService<List<Article>>.Return(context, articles);
    }

    [Mapping("articles_add", "POST", true)]
    public async Task AddArticle(HttpListenerContext context)
    {
        using var data = new StreamReader(context.Request.InputStream);
        var addArticleDto = JsonSerializer.Deserialize<AddArticleDto>(await data.ReadToEndAsync());
        var currentUserId = context.Request.Headers["user-id"];

        
        var article = await ArticleService.AddArticleAsync(addArticleDto, currentUserId);
        await ReturnService<PublicArticle>.Return(context, article.ToPublic());
    }

    [Mapping("articles_update", "PUT", true)]
    public async Task UpdateArticle(HttpListenerContext context)
    {
        using var data = new StreamReader(context.Request.InputStream);
        var updateArticleDto = JsonSerializer.Deserialize<UpdateArticleDto>(await data.ReadToEndAsync());
        var currentUserId = context.Request.Headers["user-id"];

        var article = await ArticleService.UpdateArticleAsync(updateArticleDto, currentUserId);
        await ReturnService<PublicArticle>.Return(context, article.ToPublic());
    }

    [Mapping("article_delete", "DELETE", true)]
    public async Task DeleteArticle(HttpListenerContext context)
    {
        var articleId = context.Request.QueryString["article-id"] ??
                       throw new ValidationException("article-id is missing");
        var currentUserId = context.Request.Headers["user-id"];

        await ArticleService.DeleteArticleAsync(articleId, currentUserId);
        await ReturnService<string>.Return(context, "Success");
    }
}