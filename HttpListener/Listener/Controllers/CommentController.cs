﻿using System.Net;
using System.Text.Json;
using Listener.CustomExceptions;
using Listener.Dto;
using Listener.Models.Main;
using Listener.Routing;
using Listener.Services;

namespace Listener.Controllers;

public class CommentController : IController
{
    [Mapping("comments", "GET")]
    public async Task GetArticleComments(HttpListenerContext context)
    {
        var articleId = context.Request.QueryString["article-id"];
        if (articleId is null)
            throw new ValidationException("article-id is missing");

        var comments = await CommentService.GetArticleCommentsAsync(articleId);
        await ReturnService<List<Comment>>.Return(context, comments);
    }

    [Mapping("comment_add", "POST", true)]
    public async Task AddComment(HttpListenerContext context)
    {
        using var data = new StreamReader(context.Request.InputStream);
        var addCommentDto = JsonSerializer.Deserialize<AddCommentDto>(await data.ReadToEndAsync());
        var currentUserId = context.Request.Headers["user-id"];

        var comment = await CommentService.AddCommentAsync(addCommentDto, currentUserId);
        await ReturnService<Comment>.Return(context, comment);
    }

    [Mapping("comment_edit", "PUT", true)]
    public async Task EditComment(HttpListenerContext context)
    {
        using var data = new StreamReader(context.Request.InputStream);
        var editCommentDto = JsonSerializer.Deserialize<EditCommentDto>(await data.ReadToEndAsync());
        var currentUserId = context.Request.Headers["user-id"];

        var comment = await CommentService.EditCommentAsync(editCommentDto, currentUserId);
        await ReturnService<Comment>.Return(context, comment);
    }

    [Mapping("comment_delete", "DELETE", true)]
    public async Task DeleteComment(HttpListenerContext context)
    {
        var commentId = context.Request.QueryString["comment-id"];
        if (commentId is null)
            throw new ValidationException("comment-id is missing");
        var currentUserId = context.Request.Headers["user-id"];

        await CommentService.DeleteCommentAsync(commentId, currentUserId);
        await ReturnService<string>.Return(context, "Success");
    }
}