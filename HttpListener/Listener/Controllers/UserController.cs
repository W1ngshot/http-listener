﻿using System.Net;
using System.Text.Json;
using Listener.CustomExceptions;
using Listener.Dto;
using Listener.Models;
using Listener.Models.Public;
using Listener.Routing;
using Listener.Services;
using StackExchange.Redis;

namespace Listener.Controllers;

public class UserController : IController
{
    [Mapping("current", "GET", true)]
    public async Task GetCurrentUser(HttpListenerContext context)
    {
        var currentUserId = context.Request.Headers["user-id"] ??
                            throw new UnauthorizedException();
        
        var user = await UserService.GetUserByIdAsync(currentUserId);
        await ReturnService<CurrentUser>.Return(context, user.ToCurrent());
    }
    
    //TODO реализовать декоратор для Serializer и там проверять на null
    [Mapping("register", "POST")]
    public async Task Register(HttpListenerContext context)
    {
        using var data = new StreamReader(context.Request.InputStream);
        var toEnd = await data.ReadToEndAsync();
        var registerDto = JsonSerializer.Deserialize<RegisterDto>(toEnd);
        await UserService.Register(registerDto);
        await ReturnService<string>.Return(context, "Success", ContentTypes.Text);
    }
    
    [Mapping("login", "POST")]
    public async Task Login(HttpListenerContext context)
    {
        using var data = new StreamReader(context.Request.InputStream);
        var loginDto = JsonSerializer.Deserialize<LoginDto>(await data.ReadToEndAsync());
        
        var user = await UserService.Login(loginDto);
        
        var sessionId = Guid.NewGuid().ToString();
        await RedisContext.Redis.StringAppendAsync(new RedisKey(sessionId), new RedisValue(user.Id));
        context.Response.Cookies.Add(new Cookie
        {
            Name = "session-id",
            Value = sessionId,
            Expires = DateTime.UtcNow.AddMinutes(30d)
        });
        await ReturnService<AuthorizedUser>.Return(context, new AuthorizedUser
        {
            Id = user.Id,
            Nickname = user.Nickname
        });
    }

    [Mapping("user", "GET")]
    public async Task GetUserById(HttpListenerContext context)
    {
        var userId = context.Request.QueryString["user-id"] ??
            throw new ValidationException("user-id is missing");

        var user = await UserService.GetUserByIdAsync(userId);
        await ReturnService<PublicUser>.Return(context, user.ToPublic());
    }

    [Mapping("user_update", "POST", true)]
    public async Task UpdateUser(HttpListenerContext context)
    {
        using var data = new StreamReader(context.Request.InputStream);
        var updateDto = JsonSerializer.Deserialize<UpdateUserDto>(await data.ReadToEndAsync());
        var currentUserId = context.Request.Headers["user-id"];

        await UserService.UpdateUser(updateDto, currentUserId);
        await ReturnService<string>.Return( context, "Success");
    }

    [Mapping("change_password", "POST", true)]
    public async Task ChangePassword(HttpListenerContext context)
    {
        using var data = new StreamReader(context.Request.InputStream);
        var passwordDto = JsonSerializer.Deserialize<ChangePasswordDto>(await data.ReadToEndAsync());
        var currentUserId = context.Request.Headers["user-id"];

        await UserService.ChangePassword(passwordDto, currentUserId);
        await ReturnService<string>.Return( context, "Success");
    }
}