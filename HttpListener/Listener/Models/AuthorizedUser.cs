﻿namespace Listener.Models;

public class AuthorizedUser
{
    public string Id { get; set; } = null!;
    public string Nickname { get; set; } = null!;
}