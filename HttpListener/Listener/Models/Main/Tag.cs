﻿namespace Listener.Models.Main;

public class Tag
{
    public string Id { get; set; } = null!;

    public string Name { get; set; } = null!;
}