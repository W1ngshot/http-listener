﻿namespace Listener.Models.Main;

public class Rate
{
    public string Id { get; set; } = null!;
    public int Rating { get; set; }
    public string UserId { get; set; } = null!;
    public string ArticleId { get; set; } = null!;
}