﻿namespace Listener.Models.Main;

public class Article
{
    public string Id { get; set; } = null!;
    public string Title { get; set; } = null!;
    public string? ShortDescription { get; set; }
    public string Text { get; set; } = null!;
    public string? TagName { get; set; }
    public string? TagId { get; set; }
    public DateTime UploadDate { get; set; }
    public string UserId { get; set; } = null!;
    public string? AssetLink { get; set; }
    public int Views { get; set; }
    public double Rate { get; set; }
}