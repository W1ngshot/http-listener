﻿namespace Listener.Models.Main;

public class Comment
{
    public string Id { get; set; } = null!;
    public string Message { get; set; } = null!;
    public DateTime Date { get; set; }
    public string? UserNickname { get; set; }
    public string UserId { get; set; } = null!;
    public string ArticleId { get; set; } = null!;
}