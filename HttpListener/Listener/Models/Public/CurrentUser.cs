﻿namespace Listener.Models.Public;

public class CurrentUser
{
    public string Id { get; set; } = null!;
    public DateTime RegistrationDate { get; set; }
    public string Nickname { get; set; } = null!;
    public string Email { get; set; } = null!;
}