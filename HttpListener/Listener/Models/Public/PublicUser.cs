﻿namespace Listener.Models.Public;

public class PublicUser
{
    public string Id { get; set; } = null!;
    public DateTime RegistrationDate { get; set; }
    public string Nickname { get; set; } = null!;
}