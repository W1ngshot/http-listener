﻿using System.Net;
using Listener.Handlers;

var listener = new HttpListener()
{
    Prefixes = { "http://localhost:8080/" }
};

listener.Start();

while (true)
{
    var context = await listener.GetContextAsync();
    _ = Task.Run(async () =>
    {
        await context.HandleExceptions();
    });
}