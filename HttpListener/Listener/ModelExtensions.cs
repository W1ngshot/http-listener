﻿using Listener.Models.Main;
using Listener.Models.Public;

namespace Listener;

public static class ModelExtensions
{
    public static PublicUser ToPublic(this User user) =>
        new()
        {
            Id = user.Id,
            Nickname = user.Nickname,
            RegistrationDate = user.RegistrationDate
        };

    public static PublicArticle ToPublic(this Article article) =>
        new()
        {
            Id = article.Id,
            Title = article.Title,
            ShortDescription = article.ShortDescription,
            Text = article.Text,
            TagId = article.TagId,
            TagName = article.TagName,
            UploadDate = article.UploadDate,
            UserId = article.UserId,
            AssetLink = article.AssetLink,
            Views = article.Views,
            Rate = article.Rate
        };
    
    public static CurrentUser ToCurrent(this User user) =>
        new()
        {
            Id = user.Id,
            Nickname = user.Nickname,
            RegistrationDate = user.RegistrationDate,
            Email = user.Email
        };
}