﻿namespace Listener.CustomExceptions;

public class NotFoundException : Exception
{
    public NotFoundException(string element)
    {
        Message = $"{element} not found";
    }

    public override string Message { get; }
}