﻿namespace Listener.CustomExceptions;

public class UnauthorizedException : Exception
{
    public UnauthorizedException()
    {
        Message = $"Not authorized";
    }

    public override string Message { get; }
}