﻿using System.ComponentModel.DataAnnotations;

namespace Listener.CustomExceptions;

public class ValidationException : Exception
{
    public ValidationException(List<ValidationResult>? exceptionMessages)
    {
        ExceptionMessages = exceptionMessages;
    }

    public ValidationException(string message) : base(message) { }
    
    public List<ValidationResult>? ExceptionMessages { get; }
}