import { Component } from '@angular/core'
import { Article } from '../shared/interfaces/article.interface'
import { ArticleService } from '../shared/article.service'
import { ActivatedRoute } from '@angular/router'

interface ArticleResponse {
  articles: Article[]
  totalCount: number
}

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss'],
})
export class ArticleListComponent {
  articles: Article[] | null = null
  totalArticleCount = 0

  currentCategory = 'popular'

  constructor(private articleService: ArticleService, route: ActivatedRoute) {
    this.currentCategory = route.snapshot.data['category'] as string

    route.data.subscribe(data => {
      this.currentCategory = data['category']
      this._updateArticles(this.currentCategory)
    })
  }

  private _updateArticles(category: string) {
    const request =
      category === 'my'
        ? this.articleService.getUserArticles('')
        : this.articleService.getArticlesByCategory(category)

    request.then((articles) => {
      this.articles = articles
      this.totalArticleCount = articles.length
    })
  }

  get title() {
    switch (this.currentCategory) {
      case 'popular':
        return 'Популярное'
      case 'mostviewed':
        return 'Самые просматриваемые'
      case 'new':
        return 'Новое'
      case 'my':
      default:
        return 'Мои статьи'
    }
  }
}
