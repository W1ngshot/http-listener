import { HttpErrorResponse } from '@angular/common/http'
import { Component } from '@angular/core'
import { FormControl, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'

import { ArticleService } from '../shared/article.service'
import { AuthService } from '../shared/auth.service'
import { Article } from '../shared/interfaces/article.interface'
import { Comment } from '../shared/interfaces/comment.interface'
import { UserService } from '../shared/user.service'

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
})
export class ArticleComponent {
  article: Article | null = null
  commentInputControl = new FormControl(null, [Validators.required])
  comments: Comment[] | null = null
  isCommentFormBusy = false
  authorNickname: string = ''

  constructor(
    private articleService: ArticleService,
    private authService: AuthService,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    const id = route.snapshot.params['id']
    if (!id) router.navigateByUrl('/')

    this._fetchArticleData(id)
  }

  private _fetchArticleData(articleId: string) {
    this.articleService
      .getArticleById(articleId)
      .then(value => {
        this.article = value
        this.userService.getAccountById(value.UserId).then(user => {
          this.authorNickname = user.Nickname
        })
      })
      .catch((error: HttpErrorResponse) => {
        if (error.status === 404) {
          this.router.navigateByUrl('/')
        }
      })

    this.articleService.getCommentsForArticle(articleId).then(comments => (this.comments = comments))
  }

  get isCommentSubmitButtonDisabled() {
    return !this.authService.isLoggedIn || this.commentInputControl.invalid || this.isCommentFormBusy
  }

  async onCommentAdd(event: Event) {
    event.preventDefault()

    if (this.isCommentFormBusy) return
    if (this.commentInputControl.invalid) return
    if (!this.article) return

    this.isCommentFormBusy = true
    const comment = await this.articleService.addCommentForArticle(this.article.Id, this.commentInputControl.value)
    this.comments?.unshift(comment)
    this.commentInputControl.setValue(null)
    this.isCommentFormBusy = false
  }
}
