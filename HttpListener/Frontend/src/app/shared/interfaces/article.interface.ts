export interface Article {
  Id: string
  Title: string
  ShortDescription: string
  Text: string
  UploadDate: string
  AssetLink: string
  TagId: string

  Views: number

  UserId: string
}
