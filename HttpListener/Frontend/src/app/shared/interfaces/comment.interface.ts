export interface Comment {
  Id: number
  Message: string
  Date: string
  ArticleId: string
  UserId: number
  UserNickname: string
  UserAvatarLink: string
}
