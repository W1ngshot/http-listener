import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { firstValueFrom } from 'rxjs'

import { baseApiUrl } from '../app.config'
import { ArticleService } from './article.service'
import { AuthService } from './auth.service'
import { Account } from './interfaces/account.interface'
import { User } from './interfaces/user.interface'

@Injectable({ providedIn: 'root' })
export class UserService {
  private _usersCache = new Map<string, User>();

  constructor(private http: HttpClient, private authService: AuthService, private articleService: ArticleService) {}

  async getAccountById(id: string) {
    const cachedUser = this._usersCache.get(id);
    if (cachedUser) return cachedUser

    if (this.authService.myAccount.value && this.authService.myAccount.value.Id === id)
      return this.authService.myAccount.value

    const response = await firstValueFrom(this.http.get<User>(`${baseApiUrl}/user?user-id=${id}`))
    this._usersCache.set(response.Id, response);
    return response
  }
}
