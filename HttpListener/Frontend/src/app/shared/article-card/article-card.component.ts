import { Component, Input } from '@angular/core'
import { UserService } from '../user.service'

@Component({
  selector: 'app-article-card',
  templateUrl: './article-card.component.html',
  styleUrls: ['./article-card.component.scss'],
})
export class ArticleCardComponent {
  @Input() isViewsShown: boolean = false
  @Input() views: number = 0
  @Input() userId: string = ''
  @Input() previewUrl: string = 'assets/missing-image.png'
  @Input() title: string = 'Article'
  @Input() shortDescription: string = 'Lorem ipsum dolor sit amet'
  @Input() tagId: string = ''
  @Input() isEditable: boolean = false
  @Input() id: string = ''

  userNickname: string = ''

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userService.getAccountById(this.userId).then(user => {
      this.userNickname = user.Nickname
    })
  }

  get link() {
    return `/article/${this.id}/`
  }

  get userLink() {
    return `/profile/${this.userId}`
  }

  get editLink() {
    return `/article/edit/${this.id}`
  }
}
