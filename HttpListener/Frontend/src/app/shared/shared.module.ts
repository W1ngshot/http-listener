import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { SearchComponent } from './search/search.component'
import { ArticleCardComponent } from './article-card/article-card.component'
import { HttpClientModule } from '@angular/common/http'
import { ReactiveFormsModule } from '@angular/forms'
import { UserAvatarComponent } from './user-avatar/user-avatar.component'
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    SearchComponent,
    ArticleCardComponent,
    UserAvatarComponent,
  ],
  imports: [CommonModule, HttpClientModule, ReactiveFormsModule, RouterModule],
  exports: [
    SearchComponent,
    ArticleCardComponent,
    UserAvatarComponent,
  ],
})
export class SharedModule {}
