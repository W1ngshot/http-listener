import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaderResponse } from '@angular/common/http'
import { firstValueFrom } from 'rxjs'

import { baseApiUrl } from '../app.config'
import { Article } from './interfaces/article.interface'
import { NewArticle } from './interfaces/new-article.interface'
import { Comment } from './interfaces/comment.interface'
import { AuthService } from './auth.service'

type ArticlesResponse = Article[]

interface MainPageArticles {
  popularArticles: Article[]
  bestArticles: Article[]
  latestArticles: Article[]
}

@Injectable({
  providedIn: 'root',
})
export class ArticleService {
  private _articlesCache = new Map<string, Article>()

  constructor(private http: HttpClient, private authService: AuthService) {}

  async getPopularArticles() {
    const response = await firstValueFrom(this.http.get<ArticlesResponse>(`${baseApiUrl}/Article/Popular`))

    for (const article of response) {
      this.addToCache(article)
    }

    return response
  }

  async getMainPageArticles() {
    const response = await firstValueFrom(this.http.get<MainPageArticles>(`${baseApiUrl}/Main`))

    for (const article of response.bestArticles) this.addToCache(article)
    for (const article of response.latestArticles) this.addToCache(article)
    for (const article of response.latestArticles) this.addToCache(article)

    return response
  }

  async createNewArticle(article: NewArticle) {
    const response = await firstValueFrom(
      this.http.post<Article>(
        `${baseApiUrl}/articles_add`,
        {
          Title: article.title,
          Text: article.text,
          ShortDescription: article.shortDescription,
          AssetLink: article.pictureLink,
          TagId: '75ac834a-c598-40b6-8130-1cd7286e4dbf',
        },
        {},
      ),
    )

    this.addToCache(response)

    return response
  }

  async getArticleById(id: string) {
    const cachedArticle = this._articlesCache.get(id)
    if (cachedArticle) return cachedArticle

    const response = await firstValueFrom(this.http.get<Article>(`${baseApiUrl}/article?article-id=${id}`))
    this.addToCache(response)

    return response
  }

  async updateArticle(id: string, article: NewArticle) {
    const response = await firstValueFrom(
      this.http.put<Article>(
        `${baseApiUrl}/articles_update`,
        {
          Id: id,
          Title: article.title,
          ShortDescription: article.shortDescription,
          Text: article.text,
          AssetLink: article.pictureLink,
          TagId: '75ac834a-c598-40b6-8130-1cd7286e4dbf',
        },
        {},
      ),
    )
    this.addToCache(response)

    return response
  }

  addToCache(article: Article) {
    this._articlesCache.set(article.Id, article)
  }

  async searchForArticles(query: string) {
    const response = await firstValueFrom(
      this.http.get<ArticlesResponse>(`${baseApiUrl}/articles_search?search=${query}&category=new`),
    )
    for (const article of response) this.addToCache(article)
    return response
  }

  async getArticlesByCategory(category: string) {
    const response = await firstValueFrom(
      this.http.get<ArticlesResponse>(`${baseApiUrl}/articles_category?category=${category}`),
    )
    for (const article of response) this.addToCache(article)
    return response
  }

  async getUserArticles(userId: string) {
    const response = await firstValueFrom(this.http.get<ArticlesResponse>(`${baseApiUrl}/articles_user?user-id=${userId}`, {}))
    for (const article of response) this.addToCache(article)
    return response
  }

  async getCommentsForArticle(articleId: string) {
    return await firstValueFrom(this.http.get<Comment[]>(`${baseApiUrl}/comments?article-id=${articleId}`, {}))
  }

  async addCommentForArticle(articleId: string, message: string) {
    return await firstValueFrom(
      this.http.post<Comment>(`${baseApiUrl}/comment_add`, { ArticleId: articleId, Message: message }),
    )
  }
}
