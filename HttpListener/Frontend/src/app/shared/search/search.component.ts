import { Component, Input, OnInit } from '@angular/core'
import { FormControl } from '@angular/forms'
import { Router } from '@angular/router'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  @Input() initialValue?: string

  inputControl = new FormControl(this.initialValue)

  constructor(private router: Router) {}

  ngOnInit() {
    this.inputControl.setValue(this.initialValue)
  }

  onFormSubmit(event: SubmitEvent) {
    event.preventDefault()
    const query = this.inputControl.value ?? ''
    this.router.navigateByUrl(`/search?q=${query}`)
  }
}
