import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { BehaviorSubject, firstValueFrom } from 'rxjs'

import { baseApiUrl } from '../app.config'
import { Account } from './interfaces/account.interface'
import { User } from './interfaces/user.interface'

type JWToken = string

interface RegisterPayload {
  nickname: string
  email: string
  password: string
  confirmPassword: string
}

interface LoginPayload {
  email: string
  password: string
}

@Injectable({ providedIn: 'root' })
export class AuthService {
  private _isBusy = false
  private _isInitialized = false
  private _myAccount: BehaviorSubject<User | null>

  constructor(private http: HttpClient) {
    this.init()
    this._myAccount = new BehaviorSubject<User | null>(null)
  }

  get isBusy() {
    return this._isBusy
  }

  get isInitialized() {
    return this._isInitialized
  }

  get myAccount() {
    return this._myAccount
  }

  get isLoggedIn() {
    return !!this._myAccount.value
  }

  init() {
    firstValueFrom(this.http.get<User>(`${baseApiUrl}/current`, { withCredentials: true }))
      .then(account => {
        this._myAccount.next(account)
        this._isInitialized = true
      })
      .catch((error: HttpErrorResponse) => {
        this._isInitialized = true
      })
  }

  register(payload: RegisterPayload) {
    this._isBusy = true
    const response = firstValueFrom(
      this.http.post(
        `${baseApiUrl}/register`,
        {
          Email: payload.email,
          Nickname: payload.nickname,
          Password: payload.password,
        },
        { responseType: 'text' },
      ),
    )
      .then(() => {
        this._isBusy = false
        this.init()
      })
      .catch((error: HttpErrorResponse) => {
        this._isBusy = false
        return Promise.reject(error)
      })

    return response
  }

  login(payload: LoginPayload) {
    this._isBusy = true
    const response = firstValueFrom(
      this.http.post(
        `${baseApiUrl}/login`,
        {
          Email: payload.email,
          Password: payload.password,
        },
        { responseType: 'text', withCredentials: true },
      ),
    )
      .then(() => {
        this._isBusy = false
        this.init()
      })
      .catch((error: HttpErrorResponse) => {
        this._isBusy = false
        return Promise.reject(error)
      })

    return response
  }

  logout() {
    this._myAccount.next(null)
  }
}
