import { Component } from '@angular/core'
import { ArticleService } from '../shared/article.service'
import { Article } from '../shared/interfaces/article.interface'

interface ArticleSection {
  name: string
  moreLink: string
  articleRows: Article[][]
}

@Component({
  selector: 'app-home-view',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {}
