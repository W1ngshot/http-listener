import { Component } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { ArticleService } from '../shared/article.service'
import { Article } from '../shared/interfaces/article.interface'

@Component({
  selector: 'app-search-view',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent {
  searchInitialValue: string = ''

  articles: Article[] | null = null
  totalArticleCount = 0

  constructor(private route: ActivatedRoute, private articleService: ArticleService) {
    this._handleQueryParams()
  }

  private _handleQueryParams() {
    this.route.queryParams.subscribe(queryParams => {
      const query = queryParams['q'] ?? ''

      this.searchInitialValue = query


      this.articleService.searchForArticles(query).then(articles => {
        this.articles = articles
        this.totalArticleCount = articles.length
      })
    })
  }
}
