import { HttpErrorResponse } from '@angular/common/http'
import { Component } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ArticleService } from '../shared/article.service'
import { AuthService } from '../shared/auth.service'
import { Article } from '../shared/interfaces/article.interface'
import { User } from '../shared/interfaces/user.interface'
import { UserService } from '../shared/user.service'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent {
  currentUser: User | null = null
  currentUserArticles: Article[] = []
  isSelfProfile = false
  totalArticleCount = 0
  currentUserArticleRows: Article[][] = []

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private articleService: ArticleService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    if (route.snapshot.params['id']) {
      const id = route.snapshot.params['id']
      if (!id) router.navigateByUrl('/')

      this._redirectOnMyProfile(id)
      this._fetchAccount(id)
    } else {
      this._setMyAccount()
    }
  }

  private _redirectOnMyProfile(userId: string) {
    this.authService.myAccount.subscribe(account => {
      if (!account) return
      if (account.Id === userId)
        this.router.navigateByUrl('/profile', {
          replaceUrl: true,
        })
    })
  }

  private _fetchAccount(userId: string) {
    this.userService
      .getAccountById(userId)
      .then(account => {
        this.currentUser = account
        this.articleService.getUserArticles(account.Id).then(articles => {
          this.currentUserArticles = articles
          this.totalArticleCount = articles.length
          this._setCurrentUserArticleRows()
        })
      })
      .catch((error: HttpErrorResponse) => {
        if (error.status !== 0) this.router.navigateByUrl('/')
      })
  }

  private _setMyAccount() {
    this.authService.myAccount.subscribe(account => {
      if (!account) return

      this.currentUser = account
      this.articleService.getUserArticles(account.Id).then(articles => {
        this.currentUserArticles = articles
        this.totalArticleCount = articles.length
        this._setCurrentUserArticleRows()
      })
    })

    this.isSelfProfile = true
  }

  private _setCurrentUserArticleRows() {
    const result: Article[][] = []

    for (let i = 0; i < this.currentUserArticles.length; i += 2) {
      const row = [this.currentUserArticles[i]]
      if (i + 1 < this.currentUserArticles.length) row.push(this.currentUserArticles[i + 1])
      result.push(row)
    }

    this.currentUserArticleRows = result
  }

  onLogout(event: Event) {
    event.preventDefault()
    this.authService.logout()
    this.router.navigateByUrl('/')
  }
}
