create table if not exists users
(
    id text primary key,
    email text not null,
    registration_date date,
    nickname text not null,
    password_hash text not null,
    password_salt text not null
);

create table if not exists tags
(
    id text primary key,
    name text not null
);

create table if not exists articles
(
    id text primary key,
    title text not null,
    short_description text,
    text text not null,
    tag text references tags (id) on delete set null on update cascade,
    upload_date timestamp with time zone not null,
    user_id text references users (id) on delete cascade on update cascade not null,
    asset_link text,
    views integer default 0 not null
);

create table if not exists rates
(
    id text primary key,
    rating integer not null,
    user_id text references users (id) on delete no action on update cascade not null,
    article_id text references articles (id) on delete cascade on update cascade not null
);

create table if not exists comments
(
    id text primary key,
    message text not null,
    date timestamp with time zone not null,
    user_id text references users (id) on delete no action on update cascade not null,
    article_id text references articles (id) on delete cascade on update cascade not null
);

drop table comments;
drop table rates;
drop table articles;
drop table tags;
drop table users;